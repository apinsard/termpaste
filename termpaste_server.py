#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import html
import random
import re
import string

from flask import Flask, request, redirect, url_for, make_response
import peewee

db = peewee.SqliteDatabase('termpaste.db')
app = Flask(__name__)


class BaseModel(peewee.Model):
    class Meta:
        database = db


class Theme(peewee.Model):

    name = peewee.CharField(unique=True, max_length=30)
    slug = peewee.CharField(unique=True, max_length=20)
    stylesheet = peewee.TextField()


class Paste(peewee.Model):

    key = peewee.FixedCharField(max_length=4, unique=True, default=(
        lambda: ''.join(random.choices(string.ascii_letters + string.digits, k=4))
    ))
    content = peewee.TextField()
    expiry = peewee.DateTimeField(null=True, default=lambda: datetime.now() + timedelta(days=7))


class ColorParser:

    def __init__(self):
        self.state = {}

    def substitute(self, m):
        codes = map(int, m.group(1).split(';'))
        for code in codes:
            if code == 0:
                self.state = {}
            elif code < 10:
                self.state[code] = code
            else:
                self.state[code // 10] = code
        return '</span><span class="{}">'.format(' '.join(
            'c{}'.format(v) for v in self.state.values()
        ))


def get_stylesheet(theme=None):
    if theme:
        try:
            return Theme.get(Theme.slug == theme).stylesheet
        except:
            pass
    return (
        'body { background-color: black; }'
        'nav { color: #777; font-family: monospace; text-align: right; }'
        'nav a { color: #777; }'
        '#paste-area { font-family: monospace; background-color: black; color: white; }'
        '.c0 { color: white; }'
        '.c1 { font-weight: bold; }'
        '.c32, .c92 { color: green; }'
        '.c33, .c93 { color: yellow; }'
        '.c34, .c94 { color: blue; }'
        '.c36, .c96 { color: cyan; }'
    )


@app.route('/', methods=['GET', 'POST'])
def new_paste():
    if request.method == 'POST':
        expiry = None
        if 'expiry' in request.form:
            expiry = datetime.now() + timedelta(days=int(request.form['expiry']))
        while True:
            try:
                paste = Paste.create(content=request.form['content'], expiry=expiry)
            except peewee.IntegrityError:
                # Duplicate random key, try with another random key
                continue
            break
        return redirect(url_for('show_paste', key=paste.key))
    else:
        return 'termpaste'


@app.route('/<key>')
def show_paste(key):
    paste = Paste.get(Paste.key == key)
    if paste.expiry and paste.expiry < datetime.now():
        response = make_response("This paste has expired")
        response.headers['Content-Type'] = 'text/plain'
        return response
    content = html.escape(paste.content)
    cp = ColorParser()
    content = '<span class="c0">{}</span>'.format(
        re.sub(r'\033\[((?:[0-9]+)(?:;[0-9]+)*)m', cp.substitute, content)
    )
    content = content.replace('\n', '<br>')
    head = (
        '<meta charset="utf-8">'
        '<title>Termpaste</title>'
        '<style type="text/css">{stylesheet}</style>'
    ).format(stylesheet=get_stylesheet(request.args.get('theme')))
    return (
        '<!doctype html><html><head>{head}</head><body>'
        '<nav><a href="{raw_url}">Raw</a> (<a href="{txt_url}">No color</a>)</nav>'
        '<pre id="paste-area">{content}</pre>'
        '</body></html>'
    ).format(
        head=head, content=content, raw_url=url_for('raw_paste', key=key),
        txt_url=url_for('txt_paste', key=key),
    )


@app.route('/<key>.raw')
def raw_paste(key):
    paste = Paste.get(Paste.key == key)
    response = make_response(paste.content)
    response.headers['Content-Type'] = 'text/plain'
    return response


@app.route('/<key>.txt')
def txt_paste(key):
    paste = Paste.get(Paste.key == key)
    content = paste.content
    content = re.sub(r'\033\[((?:[0-9]+)(?:;[0-9]+)*)m', '', content)
    response = make_response(content)
    response.headers['Content-Type'] = 'text/plain'
    return response
